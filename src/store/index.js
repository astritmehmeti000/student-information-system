import { createStore } from 'vuex';

import studentsModule from './modules/students/index.js';
import authModule from './modules/auth/index.js';

const store = createStore({
  modules: {
    students: studentsModule,
    auth: authModule,
  },
});

export default store;
