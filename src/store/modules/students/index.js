import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters';

export default {
  namespaced: true,
  state() {
    return {
      students: [],
      qty: 0,
    };
  },
  mutations,
  actions,
  getters,
};
