export default {
  registerStudent(context, data) {
    let studentDetails = JSON.parse(localStorage.getItem('students')) || [];

    const studentData = {
      id: studentDetails.length.toString(),
      firstName: data.first,
      lastName: data.last,
      dateOfBirth: data.dob,
      municipality: data.municipality,
    };

    studentDetails.push(studentData);
    let students = JSON.stringify(studentDetails);
    localStorage.setItem('students', students);

    context.commit('registerStudent', studentData);
  },

  loadStudents(context) {
    const responseData = JSON.parse(localStorage.getItem('students'));
    const students = [];
    for (const key in responseData) {
      const student = {
        id: key,
        firstName: responseData[key].firstName,
        lastName: responseData[key].lastName,
        dateOfBirth: responseData[key].dateOfBirth,
        municipality: responseData[key].municipality,
      };
      students.push(student);
    }
    context.commit('allStudents', students);
  },

  updateStudents(context, payload) {
    const students = JSON.parse(localStorage.getItem('students'));
    let studentId =  students.findIndex((student) => student.id === payload.id);
    students[studentId] = payload;
    localStorage.setItem('students', JSON.stringify(students));
    context.commit('allStudents',students);
  },


  deleteStudents(context, payload) {
    let students = JSON.parse(localStorage.getItem('students'));
    students.splice(payload, 1);

    const studentData = [];

    for (const key in students) {
      const student = {
        id: key,
        firstName: students[key].firstName,
        lastName: students[key].lastName,
        dateOfBirth: students[key].dateOfBirth,
        municipality: students[key].municipality,
      };
      studentData.push(student);
      console.log(studentData);
    }
    localStorage.setItem('students', JSON.stringify(studentData));
    context.commit('allStudents', studentData);
  },
};
