import { createRouter, createWebHistory } from 'vue-router';

import StudentsList from './pages/students/StudentsList.vue';
import NotFound from './pages/NotFound.vue';
import store from './store/index.js';

const StudentRegistration = () => import('./pages/students/StudentRegistration.vue')
const EditStudents = () => import('./pages/students/EditStudents.vue');
const DeleteStudents = () => import('./pages/students/DeleteStudents.vue');
const UserAuth = () => import('./pages/auth/UserAuth.vue');

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/auth' },
    {
      path: '/students',
      component: StudentsList,
      meta: { requiresAuth: true },
      children: [
        { path: 'edit/:id', component: EditStudents, meta: { requiresAuth: true }, props: true },
        { path: 'delete/:id', component: DeleteStudents, meta: { requiresAuth: true }, props: true },
      ],
    },

    { path: '/register', component: StudentRegistration, meta: { requiresAuth: true } },
    { path: '/auth', component: UserAuth, meta: { requiresUnauth: true } },
    { path: '/:notFound(.*)', component: NotFound },
  ],
});

router.beforeEach(function(to, _, next){
  if(to.meta.requiresAuth && !store.getters.isAuthenticated) {
    next('/auth');
  } else if (to.meta.requiresUnauth && store.getters.isAuthenticated) {
    next('/students');
  }else {
    next();
  }
});

export default router;
