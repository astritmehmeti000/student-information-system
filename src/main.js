import { createApp, defineAsyncComponent } from 'vue';
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import router from './router.js';
import store from './store/index.js';
import App from './App.vue';

const BaseDialog = defineAsyncComponent(()=> import('./components/layouts/BaseDialog.vue'));
const BaseCard = defineAsyncComponent(()=> import('./components/layouts/BaseCard.vue'));
const BaseButton = defineAsyncComponent(()=> import('./components/layouts/BaseButton.vue'));
const app = createApp(App);

app.use(router);
app.use(store);

app.component('base-card', BaseCard);
app.component('base-button', BaseButton);
app.component('base-dialog', BaseDialog);

app.mount('#app');
